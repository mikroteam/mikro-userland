/*
 * File: process.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Process register in VFS
 *
 */

#include <process.hh>

Process::Process(File* current)
 : _current_d (current)
{
  for (size_t i = 0; i < MAX_FD; i++)
    _fds[i] = (File*)NULL;
}

int Process::registerFile(File* f)
{
  for (size_t i = 0; i < MAX_FD; i++)
    if (_fds[i] == NULL)
    {
      _fds[i] = f;
      return i;
    }

  return -1;
}

int Process::unregisterFile(File* f)
{
  for (size_t i = 0; i < MAX_FD; i++)
    if (_fds[i] == f)
    {
      _fds[i] = (File*)NULL;
      return 0;
    }

  return -1;
}
