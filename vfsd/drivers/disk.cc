/*
 * File: disk.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: ATA PIO 48 bits driver
 *
 */

#include <mikro/mikro.h>
#include <stdio.h>
#include <stdlib.h>
#include <fs/ext2/ext2.hh>

#include "disk.hh"
#include "mbr.hh"

#define DATA_PORT 0x1f0
#define DRIVE_SELECT_PORT 0x1f6
#define SECTOR_COUNT_PORT 0x1f2
#define LBA_LOW_PORT 0x1f3
#define LBA_MID_PORT 0x1f4
#define LBA_HIGH_PORT 0x1f5
#define COMMAND_PORT 0x1f7
#define STATUS_PORT 0x1f7

#define READ_CMD 0x24

#define OUTB(A, B) mikro_out((A), (B), 1)
#define INW(A, B) (B) = mikro_in((A), 2)
#define INB(A, B) (B) = mikro_in((A), 1)

Disk::Disk(enum Drive d)
  : _bs(0),
    _drive(d)
{
}

int Disk::readBlock(u64 lba, u16 count, char* cbuff)
{
  short* buff = (short*)cbuff;
  OUTB(DRIVE_SELECT_PORT, _drive);

  OUTB(SECTOR_COUNT_PORT, (u8)(count >> 8));
  OUTB(LBA_LOW_PORT, (u8)(lba >> 3 * 8));
  OUTB(LBA_MID_PORT, (u8)(lba >> 4 * 8));
  OUTB(LBA_HIGH_PORT, (u8)(lba >> 5 * 8));
  OUTB(SECTOR_COUNT_PORT, (u8)(count));
  OUTB(LBA_LOW_PORT, (u8)(lba >> 0 * 8));
  OUTB(LBA_MID_PORT, (u8)(lba >> 1 * 8));
  OUTB(LBA_HIGH_PORT, (u8)(lba >> 2 * 8));

  OUTB(COMMAND_PORT, READ_CMD);

  for (u16 i = 0; i < count; i++)
  {
    /* Polling here */
    while (1)
    {
      u8 status = 0;

      INB(STATUS_PORT, status);
      if (!(status & 0x08) && (status & 0x80))
        continue;
      break;
    }

    for (u16 j = 0; j < 256; j++)
    {
      u16 tmp = 0;

      INW(DATA_PORT, tmp);
      buff[0] = tmp;
      buff++;
    }
  }

  return 0;
}

int Disk::findPartitions()
{
  _bs = new BootSector();
  if (!_bs)
    abort_print("VFSD: Unable to allocate BootSector");

  if (readBlock(0, 1, (char*)_bs) < 0)
    abort_print("VFSD: Unable to read on disk");

  if (_bs->signature != MBR_SIGNATURE)
  {
    printf("Boot Sector is not valid.");
    return -1;
  }

  prts = new Partition*[4];
  if (!prts)
    abort_print("VFSD: no more memory");

  for (u8 i = 0; i < 4; i++)
  {
    struct PartitionEntry* current = &(_bs->part[i]);

    if (current->system_id == LINUX_PARTITION)
    {
      prts[prts_size++] = new Ext2Partition(this, current->lba_start);
      if (!prts[prts_size - 1])
        abort_print("VFSD: no more memory");
    }
  }

  return 0;
}
