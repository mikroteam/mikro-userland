/*
 * File: disk.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Disk as a Storage device
 *
 */

#ifndef DISK_HH_
# define DISK_HH_

# include <mikro/types.h>
# include <storage.hh>

struct BootSector;

enum Drive
{
  Master = 0x40,
  Slave = 0x50
};

class Disk : public Storage
{
  public:
    Disk(enum Drive d);
    virtual int readBlock(u64 lba, u16 count, char* buff);
    virtual int findPartitions();

  private:
    struct BootSector* _bs;
    enum Drive _drive;
};

#endif /* !DISK_HH_ */
