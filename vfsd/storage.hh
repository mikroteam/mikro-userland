/*
 * File: storage.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Abstract Storage device
 *
 */

#ifndef STORAGE_HH_
# define STORAGE_HH_

# include <mikro/types.h>

class Partition;

class Storage
{
  public:
    Storage()
      : prts_size(0),
        prts(0)
    {
    }
    virtual int readBlock(u64 lba, u16 count, char* buff) = 0;
    virtual int findPartitions() = 0;

    size_t prts_size;
    Partition** prts;
};

#endif /* !STORAGE_HH_ */
