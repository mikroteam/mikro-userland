/*
 * File: process.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Process register in VFS
 *
 */

#ifndef PROCESS_HH_
# define PROCESS_HH_

# define MAX_FD 255

# include <stddef.h>
# include <mikro/types.h>
# include <file.hh>

class Process
{
  public:
    Process(File* current);

    File* getCurrentDirectory()
    {
      return _current_d;
    }

    void setCurrentDirectory(File* current)
    {
      _current_d = current;
    }

    int registerFile(File* f);
    int unregisterFile(File* f);

  private:
    File* _fds[MAX_FD];
    File* _current_d;
};

#endif /* !PROCESS_HH_ */
