/*
 * File: ext2_file.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: File on ext2
 *
 */

#ifndef EXT2_FILE_HH_
# define EXT2_FILE_HH_

# include <file.hh>

struct Ext2Inode;

class Ext2File : public File
{
  public:
    Ext2File(Ext2Partition *prt, struct Ext2Inode* ino);
    ~Ext2File();

    virtual int read(char* buff, size_t off, size_t size);
    virtual size_t size();

    struct Ext2Inode* getInode()
    {
      return _ino;
    }

  private:
    struct Ext2Inode* _ino;
};

#endif /* !EXT2_FILE_HH_ */
