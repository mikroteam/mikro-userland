/*
 * File: ext2.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Minimalist read only ext2 implementation
 *
 */

#ifndef EXT2_HH_
# define EXT2_HH_

# include <mikro/types.h>

# include <partition.hh>

struct Ext2Inode;

class Ext2Partition : public Partition
{
  public:
    Ext2Partition(Storage* sto, u64 lba);
    virtual File* open(Process* p, const char* name);
    virtual int close(Process* p, File* f);

    int readBlock(u32 blk_idx, char* buff);
    u32 getBlockSize();
    u32 findBlockID(const struct Ext2Inode* file, u32 blk_nb);

  private:
    void ext2GetSuper();
    void ext2GetBlock();
    struct Ext2Inode* ext2GetInode(u32 ino_nb);
    struct Ext2Inode* ext2GetFileFromDir(const File* f,
                                         const char* name);

    u64 _lba;
    struct Ext2Super* _spb;
    struct Ext2BlockGroupEntry* _bgd;
};

#endif /* !EXT2_HH_ */
