/*
 * File: ext2_private.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Internal structs on ext2
 *
 */

#ifndef EXT2_PRIVATE_HH_
# define EXT2_PRIVATE_HH_

# include <mikro/types.h>

# define EXT2_SUPER_MAGIC 0xEF53
# define EXT2_SPB_OFFSET 2
# define EXT2_SPB_SIZE 1024

# define EXT2_BGD_OFFSET (EXT2_SPB_OFFSET + 2)

# define EXT2_S_IFDIR 0x4000
# define EXT2_NAME_MAX_LEN 255

# define EXT2_ROOT_INO 2

# define EXT2_SIMPLE_INDIR 12
# define EXT2_DOUBLE_INDIR 13
# define EXT2_TRIPLE_INDIR 14

struct Ext2Super
{
  u32 s_inodes_count;
  u32 s_blocks_count;
  u32 s_r_blocks_count;
  u32 s_free_blocks_count;
  u32 s_free_inodes_count;
  u32 s_first_data_block;
  u32 s_log_block_size;
  u32 s_log_frag_size;
  u32 s_blocks_per_group;
  u32 s_frags_per_group;
  u32 s_inodes_per_group;
  u32 s_mtime;
  u32 s_wtime;
  u16 s_mnt_count;
  u16 s_max_mnt_count;
  u16 s_magic;
  u16 s_state;
  u16 s_errors;
  u16 s_minor_rev_level;
  u32 s_lastcheck;
  u32 s_checkinterval;
  u32 s_creator_os;
  u32 s_rev_level;
  u16 s_def_resuid;
  u16 s_def_resgid;

  /* EXT2 Dynamic rev */
  u32 s_first_ino;
  u16 s_inode_size;
  u16 s_block_group_nr;
  u32 s_feature_compat;
  u32 s_feature_incompat;
  u32 s_feature_ro_compat;
  u8 s_uuid[16];
  u8 s_volume_name[16];
  u8 s_last_mounted[64];
  u32 s_algo_bitmap;

  /* Performance Hints */
  u8 s_prealloc_blocks;
  u8 s_prealloc_dir_blocks;
  u16 alignment_unused;

  /* Journaling support */
  u8 s_journal_uuid[16];
  u32 s_journal_inum;
  u32 s_journal_dev;
  u32 s_last_orphan;

  /* Directory Index Support */
  u8 s_hash_seed[16];
  u8 s_def_hash_version;
  u8 padding[3];

  /* Others options */
  u32 s_default_mount_options;
  u32 s_first_meta_bg;
  u8 reserved[760];
}__attribute__((packed));

struct Ext2BlockGroupEntry
{
  u32 bg_block_bitmap;
  u32 bg_inode_bitmap;
  u32 bg_inode_table;
  u16 bg_free_blocks_count;
  u16 bg_free_inodes_count;
  u16 bg_used_dirs_count;
  u16 bg_pad;
  u8 bg_reserved[12];
}__attribute__((packed));

struct Ext2Inode
{
  u16 i_mode;
  u16 i_uid;
  u32 i_size;
  u32 i_atime;
  u32 i_ctime;
  u32 i_mtime;
  u32 i_dtime;
  u16 i_gid;
  u16 i_links_count;
  u32 i_blocks;
  u32 i_flags;
  u32 i_osd1;
  u32 i_block[15];
  u32 i_generation;
  u32 i_file_acl;
  u32 i_dir_acl;
  u32 i_faddr;
  u8 i_osd2[12];
}__attribute__((packed));

struct Ext2DirectoryEntry
{
  u32 inode;
  u16 rec_len;
  u8 name_len;
  u8 file_type;
  char name[0];
}__attribute__((packed));



#endif /* !EXT2_PRIVATE_HH_ */
