/*
 * File: ext2.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: ext2 partition
 *
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <storage.hh>
#include <process.hh>

#include "ext2.hh"
#include "ext2_private.hh"
#include "ext2_file.hh"

Ext2Partition::Ext2Partition(Storage* sto, u64 lba)
  : Partition(sto), _lba (lba)
{
  ext2GetSuper();
  ext2GetBlock();

  struct Ext2Inode* ino = ext2GetInode(EXT2_ROOT_INO);
  _root = new Ext2File(this, ino);
  if (!_root)
    abort_print("VFSD: no more memory");
}

File* Ext2Partition::open(Process* p, const char* name)
{
  struct Ext2Inode* new_ino;
  Ext2File* new_f;

  if ((new_ino = ext2GetFileFromDir(p->getCurrentDirectory(), name)) == 0)
    return (File*)NULL;

  new_f = new Ext2File(this, new_ino);
  if (!new_f)
    abort_print("VFSD: no more memory");

  if (p->registerFile(new_f) < 0)
    abort_print("VFSD: Unable to register file");

  return new_f;
}

int Ext2Partition::close(Process* p, File* f)
{
  if (p->unregisterFile(f) < 0)
  {
    printf("Unable to unregister file");
    return -1;
  }

  delete f;
  return 0;
}

u32 Ext2Partition::getBlockSize()
{
  return (1024 << _spb->s_log_block_size);
}

void Ext2Partition::ext2GetSuper()
{
  char buff[EXT2_SPB_SIZE];

  _spb = new Ext2Super();
  if (!_spb)
    abort_print("VFSD: no more memory");

  if (_sto->readBlock(_lba + EXT2_SPB_OFFSET,
                      EXT2_SPB_SIZE / 512, buff) < 0)
    abort_print("VFSD: unable to read on disk");

  memcpy(_spb, buff, EXT2_SPB_SIZE);

  if (_spb->s_magic != EXT2_SUPER_MAGIC)
    abort_print("Invalid super block in ext2 partition");
}

void Ext2Partition::ext2GetBlock()
{
  _bgd = (struct Ext2BlockGroupEntry*)malloc(getBlockSize());
  if (!_bgd)
    abort_print("VFSD: no more memory");

  if (_sto->readBlock(_lba + EXT2_BGD_OFFSET,
                  (1024 << _spb->s_log_block_size) / 512,
                  (char*)_bgd) < 0)
    abort_print("VFSD: unable to read on disk");
}

struct Ext2Inode* Ext2Partition::ext2GetInode(u32 ino_nb)
{
  u32 bg = (ino_nb - 1) / _spb->s_inodes_per_group;
  u32 ino_idx = (ino_nb - 1) % _spb->s_inodes_per_group;
  u32 blk = _bgd[bg].bg_inode_table * getBlockSize() +
            ino_idx * _spb->s_inode_size;

  char buff[1024 << _spb->s_log_block_size];
  struct Ext2Inode* new_ino = new Ext2Inode();
  if (!new_ino)
    abort_print("VFSD: no more memory");

  if (_sto->readBlock(_lba + blk / 512, getBlockSize() / 512, buff) < 0)
    abort_print("VFSD: unable to read on disk");

  memcpy(new_ino, buff + (blk % 512), sizeof (struct Ext2Inode));

  return new_ino;
}

int Ext2Partition::readBlock(u32 ext2_blk_idx, char* buff)
{
  if (_sto->readBlock(_lba + ((getBlockSize() / 512) * ext2_blk_idx),
                      getBlockSize() / 512, buff) < 0)
    abort_print("VFSD: unable to read on disk");
  return 1;
}

struct Ext2Inode* Ext2Partition::ext2GetFileFromDir(const File* f,
                                                    const char* name)
{
  struct Ext2Inode* dir = ((Ext2File*)f)->getInode();
  if ((dir->i_mode & EXT2_S_IFDIR) == 0)
    printf("Inode does not point to a directory");

  u32 blk_size = 1024 << _spb->s_log_block_size;
  char blk_buff[blk_size];
  char *current;
  u32 blocks = dir->i_blocks / (blk_size / 512);
  u32 name_len = strlen(name);
  u32 total = dir->i_size;
  u32 current_read = 0;

  for (u32 i = 0; i < blocks; i++)
  {
    u32 blk_current = findBlockID(dir, i);
    if (blk_current == 0)
      break;

    readBlock(blk_current, blk_buff);
    current = blk_buff;

    while (1)
    {
      struct Ext2DirectoryEntry* dirent = (struct Ext2DirectoryEntry*)current;

      if (dirent->name_len == name_len)
        if (!strncmp(name, dirent->name, name_len))
          return ext2GetInode(dirent->inode);

      current_read += dirent->rec_len;
      if (current_read >= total)
        return 0;
      if ((((u32)current - (u32)blk_buff) + dirent->rec_len) >= blk_size)
        break;
      current += dirent->rec_len;
    }
  }

  return 0;
}

u32 Ext2Partition::findBlockID(const struct Ext2Inode* file, u32 blk_nb)
{
  if (blk_nb < EXT2_SIMPLE_INDIR)
    return file->i_block[blk_nb];

  u32 blk_size = getBlockSize();
  u32 blk_id_per_blk = blk_size / sizeof (u32);
  u32 buff[blk_id_per_blk];
  u32 limit = blk_id_per_blk;
  u32 pow = blk_id_per_blk;
  u16 count = 0;
  u32 current_id = EXT2_SIMPLE_INDIR;

  blk_nb -= EXT2_SIMPLE_INDIR;
  while (blk_nb > limit)
  {
    count++;
    blk_nb -= pow;
    pow *= blk_id_per_blk;
    limit += pow;
  }

  if (current_id + count > EXT2_TRIPLE_INDIR)
    printf("Incorrect block id");

  memcpy(buff, (void*)file->i_block, 15 * sizeof (u32));
  current_id += count;
  if (readBlock(buff[current_id], (char*)buff) < 0)
    abort_print("VFSD: unable to read on disk");

  while (count)
  {
    pow /= blk_id_per_blk;
    current_id = blk_nb / pow;
    blk_nb = blk_nb % pow;
    readBlock(buff[current_id], (char*)buff);
    --count;
  }

  return buff[blk_nb];
}
