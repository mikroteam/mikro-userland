/*
 * File: ext2_file.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: File operation on ext2
 *
 */

#include <string.h>
#include <stdlib.h>

#include "ext2.hh"
#include "ext2_private.hh"
#include "ext2_file.hh"

#define MIN(A,B) ((A) > (B) ? (B) : (A))

Ext2File::Ext2File(Ext2Partition *prt, struct Ext2Inode* ino)
  : File(prt),
    _ino (ino)
{
}

Ext2File::~Ext2File()
{
  delete _ino;
}

int Ext2File::read(char* buff, size_t off, size_t size)
{
  Ext2Partition* ext2_prt = reinterpret_cast<Ext2Partition*>(_prt);
  u32 blk_size = ext2_prt->getBlockSize();
  u32 current_block_nb = off / blk_size;

  off %= blk_size;
  while (size)
  {
    u32 blk_current = ext2_prt->findBlockID(_ino, current_block_nb++);
    if (blk_current == 0)
      break;

    if (size > blk_size && off == 0)
    {
      if (ext2_prt->readBlock(blk_current, buff) < 0)
        abort_print("VFSD: unable to read on disk");

      size -= blk_size;
      buff += blk_size;
    }
    else
    {
      char tmp_buff[blk_size];
      u32 to_read = MIN(blk_size - off, size);

      if (ext2_prt->readBlock(blk_current, tmp_buff) < 0)
        abort_print("VFSD: unable to read on disk");
      memcpy(buff, tmp_buff + off, to_read);
      size -= to_read;
      buff += to_read;
      off = 0;
    }
  }

  return 1;
}

size_t Ext2File::size()
{
  return _ino->i_size;
}
