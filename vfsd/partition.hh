/*
 * File: partition.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Partition
 *
 */

#ifndef PARTITION_HH_
# define PARTITION_HH_

class File;
class Storage;
class Process;

class Partition
{
  public:
    Partition(Storage* sto)
      : _sto (sto)
    {
    }
    virtual File* open(Process* p, const char* name) = 0;
    virtual int close(Process* p, File* f) = 0;
    File* getRoot()
    {
      return _root;
    }

  protected:
    Storage* _sto;
    File* _root;
};

#endif /* !PARTITION_HH_ */
