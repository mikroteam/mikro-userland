/*
 * File: file.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Abstract File
 *
 */

#ifndef FILE_HH_
# define FILE_HH_

# include <mikro/types.h>

class Partition;

class File
{
  public:
    File(Partition* prt)
      : _prt (prt)
    {
      (void)_prt;
    }

    virtual ~File() {};

    virtual int read(char* buff, size_t off, size_t size) = 0;
    virtual size_t size() = 0;

  protected:
    Partition* _prt;
};

#endif /* !FILE_HH_ */
