/*
 * File: main.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Entry point of the vfsd
 *
 */

#include <stdio.h>

#include <file.hh>
#include <partition.hh>
#include <process.hh>
#include <drivers/disk.hh>

int main(void)
{
  Disk* d = new Disk(Drive::Master);
  d->findPartitions();

  printf("%d partitions found", d->prts_size);

  Partition* p = d->prts[0];
  Process* self = new Process(d->prts[0]->getRoot());

  File* f = p->open(self, "mikrob.conf");

  if (!f)
    printf("Unable to open file!");

  size_t size = f->size();
  char buff[size + 1];
  printf("File has a size of %d", size);
  if (!f->read(buff, 0, size))
    printf("Error while reading");
  buff[size] = 0;

  printf("Content: %s", buff);
  p->close(self, f);

  while (1)
    continue;
}
