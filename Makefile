# Makefile

-include Makefile.rules

#verbose?
ifeq ($(VERBOSE), 1)
	MAKEFLAGS =
else
	MAKEFLAGS = --silent --no-print-directory
endif

SUBDIRS	= lib/ loopd/ vfsd/

-include $(MK)Makefile.subdirs

distclean::
	$(RM) -R build* $(MNT_POINT) Makefile.rules $(MIKROB_CONFIG)

Makefile.rules:
	./configure

# EOF
