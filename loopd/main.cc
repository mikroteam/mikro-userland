/*
 * File: main.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Loop daemon, just do a loop :D
 *
 */

#include <mikro/mikro.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int main(void)
{
  char* str = new char[23];
  memcpy(str, "Userland has a malloc!", 23);
  printf("%s - %x", str, 42);
  delete[] str;

  while (1)
    continue;
}
