/*
 * File: _start.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: entry point
 *
 */

extern int main(void);

void _start()
{
  main();

  // TODO : exit process here
  while (1)
    ;
}
