/*
 * File: mman.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Mapping related functions
 *
 */

#ifndef SYS_MMAN_H_
# define SYS_MMAN_H_

# include <sys/types.h>

// FIXME: move this define to another location
# define PAGE_SHIFT 12

// Just for compat purposes
# define MAP_ANONYMOUS 1
# define MAP_PRIVATE 2
# define MAP_FAILED ((void*)0)
# define PROT_READ 1
# define PROT_WRITE 2
# define PROT_EXEC 4

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

void* mmap(void *addr, size_t len, int prot, int flags, int fd, off_t offset);
int munmap(void *addr, size_t len);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !SYS_MMAN_H_ */
