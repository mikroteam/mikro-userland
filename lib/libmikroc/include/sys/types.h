/*
 * File: types.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: basic types
 *
 */

#ifndef SYS_TYPES_H_
# define SYS_TYPES_H_

# include <mikro/types.h>

typedef i64 off_t;

#endif /* !SYS_TYPES_H_ */
