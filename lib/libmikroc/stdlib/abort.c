/*
 * File: abort.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: simple buffer implemenation for printf
 *
 */

#ifdef DEBUG
#include <mikro/mikro.h>
#endif

__attribute__((noreturn)) void abort(void)
{
  while (1)
    ;
}

__attribute__((noreturn)) void abort_print(const char* str)
{
#ifdef DEBUG
  mikro_debug(str);
#else
  (void)str;
#endif
  abort();
}
