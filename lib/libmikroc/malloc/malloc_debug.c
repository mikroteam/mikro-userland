/*
 * File: malloc_debug.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Tools to debug malloc
 *
 */

#include <stdarg.h>
#include <string.h>

# ifdef __MIKRO__
#  include <mikro/mikro.h>
# else /* __MIKRO__ */
#  include <unistd.h>
# endif

#include "malloc_debug.h"

void malloc_print_n(const char* s, size_t n)
{
# ifdef __MIKRO__
  // FIXME : will not behave correctly, we must cut the string
  (void)n;
  mikro_debug((char*)s);
# else /* __MIKRO__ */
  write(STDOUT_FILENO, s, n);
# endif
}

void malloc_print(const char* s)
{
  malloc_print_n(s, strlen(s));
}

__attribute__((noreturn)) void malloc_abort(const char* s)
{
  malloc_print(s);
#ifdef __MIKRO__
  while (1)
    ;
#else
  _exit(1);
#endif
}

static char ref[] = "0123456789abcdefghijklmnopqrstuvwxyz";

static size_t reverse_string(char* dst, const char* src, int cpt)
{
  int i = 0;

  for (cpt--; cpt >= 0; cpt--, i++)
    dst[i] = src[cpt];

  dst[i] = 0;

  return i;
}

static size_t uitoa_base(size_t n, char *s, int base)
{
  unsigned int cpt = 0;
  char buff[65];

  if (n == 0)
    buff[cpt++] = '0';

  while (n > 0)
  {
    buff[cpt++] = ref[n % base];
    n /= base;
  }

  return reverse_string(s, buff, cpt);
}

void malloc_print_nb(size_t n, int base)
{
  char buff[65];
  uitoa_base(n, buff, base);
  if (base == 16)
    malloc_print("0x");
  malloc_print(buff);
}

static int apply_format(const char format, va_list* args)
{
  if (format == 'x')
  {
    int x = va_arg(*args, int);
    malloc_print_nb(x, 16);
  }
  else if (format == 'u')
  {
    int d = va_arg(*args, int);
    malloc_print_nb(d, 10);
  }
  else if (format == 's')
  {
    char* s = va_arg(*args, char*);
    if (s)
      malloc_print(s);
    else
      malloc_print("(null)");
  }

  return 0;
}

void malloc_printf(const char* format, ...)
{
  va_list args;
  int     prev = 0;

  va_start(args, format);

  for (int i = 0; format[i]; i++)
  {
    if (format[i] == '%')
    {
      malloc_print_n(format + prev, i - prev);
      i++;
      apply_format(format[i], &args);
      prev = i + 1;
    }
  }
  malloc_print(format + prev);

  va_end(args);
}
