/*
 * File: malloc_internal.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: internal malloc structures
 *
 */

#ifndef MALLOC_INTERNAL_H_
# define MALLOC_INTERNAL_H_

#ifdef __APPLE__
# define MAP_ANONYMOUS MAP_ANON
#endif

extern volatile int malloc_spinlock;

void malloc_spinlock_lock(volatile int* exclusion);
void malloc_spinlock_unlock(volatile int* exclusion);

#define MALLOC_LOCK malloc_spinlock_lock(&malloc_spinlock)
#define MALLOC_UNLOCK malloc_spinlock_unlock(&malloc_spinlock)

#define MIN(A, B) ((A) > (B) ? (B) : (A))

#define PAGE_SHIFT 12
#define PAGE_SIZE (1 << PAGE_SHIFT)
#define MIN_SHIFT 5
#define MIN_SIZE (1 << MIN_SHIFT)

struct malloc_meta;

struct malloc_list
{
  struct malloc_meta* next;
  struct malloc_meta* prev;
};

union malloc_data
{
  struct malloc_list free;
  char user[0];
};

// This struct has to be aligned on a machine word
struct malloc_meta
{
  size_t size;
  union malloc_data data;
};

typedef struct malloc_meta meta;

extern meta* list_heads[PAGE_SHIFT - 1];

#define META_SIZE (sizeof (size_t))
#define ALIGNED_META_MAGIC 0x3

#define MAX_SHIFT_BIT ((size_t)1 << ((sizeof (size_t) * 8) - 1))
#define SET_INUSE(P) (P->size &= ~MAX_SHIFT_BIT)
#define SET_FREE(P) (P->size |= MAX_SHIFT_BIT)
#define IS_FREE(P) (P->size & MAX_SHIFT_BIT)
#define GET_SIZE(P) (P->size & ~MAX_SHIFT_BIT)
#define SET_SIZE(P, NSIZE) (P->size = IS_FREE(P) | (NSIZE))
#define IS_ALIGNED(P) (ALIGNED_META_MAGIC == (GET_SIZE(P) & ALIGNED_META_MAGIC))
#define GET_OFFSET_FROM_ALIGNED(P) (GET_SIZE(P) >> 16)

#define MALLOC_MAX_SIZE (~MAX_SHIFT_BIT)

#endif /* !MALLOC_INTERNAL_H_ */
