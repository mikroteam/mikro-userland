/*
 * File: malloc_debug.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Tools to debug malloc
 *
 */

#ifndef MALLOC_DEBUG_H_
# define MALLOC_DEBUG_H_

#ifdef DEBUG

void malloc_print(const char* s);
void malloc_print_nb(size_t n, int base);
void malloc_printf(const char* format, ...);

#endif

#if defined(DEBUG) & defined(LOG_MALLOC)
# define DPRINT(FMT, args...) malloc_printf(FMT, ##args)
#else
# define DPRINT(FMT, ...)
#endif

__attribute__((noreturn)) void malloc_abort(const char* s);

#endif /* !MALLOC_DEBUG_H_ */
