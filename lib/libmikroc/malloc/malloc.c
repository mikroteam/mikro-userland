/*
 * File: malloc.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Simple and portable malloc implementation
 *
 */

#include <sys/mman.h> // For mmap and munmap
#include <string.h> // For memcpy and memset

#include "malloc_debug.h"
#include "malloc.h"
#include "malloc_internal.h"

volatile int malloc_spinlock = 0;

void malloc_spinlock_lock(volatile int* exclusion)
{
  while (__sync_lock_test_and_set(exclusion, 1))
    while (*exclusion)
      continue;
}

void malloc_spinlock_unlock(volatile int* exclusion)
{
  __sync_lock_release(exclusion);
}

meta* list_heads[PAGE_SHIFT - 1];

__attribute__((const)) static size_t order(size_t l)
{
  size_t order = 0;
  size_t current = 1;

  if (!l)
    return 0;

  while ((l & current) == 0)
  {
    current <<= 1;
    order++;
  }

  return order;
}

__attribute__((const)) static size_t round_up_binary(size_t n)
{
  char   res = 0;
  size_t cur = 1;

  while (n > (cur << res))
    res++;

  return (res);
}

__attribute__((const)) static size_t size_to_page_number(size_t size)
{
  return ((size - 1) / PAGE_SIZE) + 1;
}

static void add_to_free_list(meta* new)
{
  meta** list_head = &list_heads[order(GET_SIZE(new)) - MIN_SHIFT];

  SET_FREE(new);
  new->data.free.next = *list_head;
  new->data.free.prev = NULL;
  if (*list_head)
    (*list_head)->data.free.prev = new;
  *list_head = new;
}

static void remove_from_free_list(meta* to_del)
{
  meta** list_head = &list_heads[order(GET_SIZE(to_del)) - MIN_SHIFT];

  SET_INUSE(to_del);
  if (*list_head == to_del)
    *list_head = to_del->data.free.next;
  if (to_del->data.free.next)
    to_del->data.free.next->data.free.prev = to_del->data.free.prev;
  if (to_del->data.free.prev)
    to_del->data.free.prev->data.free.next = to_del->data.free.next;
}

static meta* create_meta(void* p, size_t l)
{
  meta* m = (meta*)p;

  SET_SIZE(m, l);
  SET_INUSE(m);
  return m;
}

static void create_buddy(meta* b)
{
  size_t buddy = (size_t)b;
  size_t size = GET_SIZE(b);

  size >>= 1;
  SET_SIZE(b, size);
  buddy += size;
  meta* buddy_meta = (meta*)buddy;
  SET_SIZE(buddy_meta, size);
  add_to_free_list(buddy_meta);
}

static meta* find_buddy(meta* s)
{
  size_t addr = (size_t)s;
  addr ^= GET_SIZE(s);

  return (meta*)(addr);
}

static meta* get_meta(const void* ptr)
{
  const char* cptr = ptr;
  return (meta*)(cptr - META_SIZE);
}

static meta* get_meta_from_aligned(meta* ptr)
{
  char* cptr = (char*)ptr;
  return (meta*)(cptr - GET_OFFSET_FROM_ALIGNED(ptr));
}

static meta* split(meta* b, size_t s)
{
  if (GET_SIZE(b) == s || GET_SIZE(b) == MIN_SIZE)
    return b;

  create_buddy(b);
  return split(b, s);
}

static meta* fusion(meta* b, size_t s)
{
  if (GET_SIZE(b) >= PAGE_SIZE || GET_SIZE(b) == s)
    return b;

  meta* buddy = find_buddy(b);
  if (!IS_FREE(buddy) || GET_SIZE(buddy) != GET_SIZE(b))
    return b;

  remove_from_free_list(buddy);
  meta* left_buddy = MIN(b, buddy);
  SET_SIZE(left_buddy, GET_SIZE(left_buddy) << 1);
  return fusion(left_buddy, s);
}

static void* large_malloc(size_t size)
{
  void* ptr = mmap(0, size, PROT_READ | PROT_WRITE,
                    MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (ptr == MAP_FAILED)
  {
    malloc_abort("[MALLOC] MMAP ERROR\n");
    return NULL;
  }
  meta* meta = create_meta(ptr, size);

  return meta->data.user;
}

static meta* find_free_block(size_t size)
{
  size_t current = order(size) - MIN_SHIFT;
  size_t max = PAGE_SHIFT - MIN_SHIFT;

  for (; current < max; current++)
  {
    meta* block = list_heads[current];
    if (block)
    {
      remove_from_free_list(block);
      return block;
    }
  }

  return NULL;
}

static size_t internal_size(size_t size)
{
  size += META_SIZE;

  if (size >= PAGE_SIZE)
    return size_to_page_number(size) * PAGE_SIZE;

  if (size < MIN_SIZE)
    size = MIN_SIZE;
  size = 1 << round_up_binary(size);
  return size;
}

static void* buddy_malloc(size_t size)
{
  MALLOC_LOCK;
  meta* block = find_free_block(size);
  if (block)
  {
    block = split(block, size);
    MALLOC_UNLOCK;
    return block->data.user;
  }

  void* ptr = mmap(0, PAGE_SIZE, PROT_READ | PROT_WRITE,
                    MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (ptr == MAP_FAILED)
  {
    malloc_abort("[MALLOC] MMAP ERROR\n");
    return NULL;
  }

  meta* meta = create_meta(ptr, PAGE_SIZE);
  meta = split(meta, size);
  MALLOC_UNLOCK;
  return meta->data.user;
}

void free(void* ptr)
{
  if (ptr == NULL)
    return;

  DPRINT("free\t: ptr = %x", ptr);

  meta* meta = get_meta(ptr);
  if (IS_ALIGNED(meta))
    meta = get_meta_from_aligned(meta);

  MALLOC_LOCK;
  meta = fusion(meta, PAGE_SIZE);
  if (GET_SIZE(meta) >= PAGE_SIZE)
  {
    MALLOC_UNLOCK;
    if (munmap(meta, GET_SIZE(meta)) != 0)
      malloc_abort("[FREE] MUNMAP ERROR\n");

    DPRINT(" ==> munmap\n");
    return;
  }

  add_to_free_list(meta);
  MALLOC_UNLOCK;
  DPRINT(" ==> recycle\n");
}

void* malloc(size_t size)
{
  if (!size)
    return NULL;

  DPRINT("malloc\t: size = %u, ", size);

  void* ptr = NULL;
  size = internal_size(size);
  DPRINT("internal size = %u ", size);

  if (size > MALLOC_MAX_SIZE)
    return NULL;
  if (size > PAGE_SIZE)
  {
    ptr = large_malloc(size);
    goto end;
  }

  ptr = buddy_malloc(size);
end:
  DPRINT("==> return addr %x\n", (size_t)ptr);
  return ptr;
}

void* realloc(void* ptr, size_t size)
{
  if (!ptr)
    return malloc(size);
  if (!size)
  {
    free(ptr);
    return NULL;
  }

  void* to_free = ptr;
  size_t user_size = size;
  size = internal_size(size);
  meta* meta = get_meta(ptr);

  if (size > MALLOC_MAX_SIZE)
    return NULL;

  DPRINT("realloc\t: ptr = %x, size = %u, ", ptr, user_size);
  DPRINT("internal_prev_size = %u, new_internal_size = %u, ", GET_SIZE(meta), size);

  if (size == GET_SIZE(meta))
    return ptr;

  if (size <= PAGE_SIZE && GET_SIZE(meta) <= PAGE_SIZE && !IS_ALIGNED(meta))
  {
    if (size > GET_SIZE(meta))
    {
      size_t previous_size = GET_SIZE(meta) - META_SIZE;

      MALLOC_LOCK;
      meta = fusion(meta, size);
      to_free = meta->data.user;
      if (GET_SIZE(meta) == size)
      {
        SET_INUSE(meta);
        MALLOC_UNLOCK;
        if (ptr != meta->data.user)
          memcpy(meta->data.user, ptr, previous_size);
        DPRINT("Fusion sucess => return addr %x\n", meta->data.user);
        return meta->data.user;
      }
      MALLOC_UNLOCK;
    }
    else
    {
      MALLOC_LOCK;
      meta = split(meta, size);
      MALLOC_UNLOCK;
      DPRINT("Split sucess => return addr %x\n", meta->data.user);
      return meta->data.user;
    }
  }

  DPRINT("Fallback => malloc/free\n");
  size = user_size;
  void* new_ptr = malloc(size);
  if (!new_ptr)
    return NULL;
  memcpy(new_ptr, ptr, MIN(size, GET_SIZE(meta) - META_SIZE));
  free(to_free);

  return new_ptr;
}

void* reallocf(void* ptr, size_t size)
{
  void* ret = realloc(ptr, size);

  if (!ret)
    free(ptr);
  return ret;
}

void* calloc(size_t nmenb, size_t size)
{
  size_t mem_size = nmenb * size;
  void* ptr = NULL;

  if (!mem_size)
    return NULL;

  ptr = malloc(mem_size);
  if (!ptr)
    return NULL;
  memset(ptr, 0, mem_size);
  return ptr;
}

static int large_memalign(void** ptr, size_t alignment, size_t size)
{
  size_t pages = size_to_page_number(size) * PAGE_SIZE + alignment;
  char* res;

  res = large_malloc(pages);
  DPRINT(" %u pages at addr %x, ", pages, res);
  size_t shift = alignment - ((size_t)(res - META_SIZE) % alignment) - META_SIZE;
  char* aligned = res - META_SIZE + shift;
  DPRINT("shift %u, ", shift);
  meta* aligned_meta = create_meta(aligned,  ((aligned - res) << 16) | ALIGNED_META_MAGIC);

  DPRINT("==> return addr %x\n", aligned_meta->data.user);
  *ptr = aligned_meta->data.user;
  return 0;
}

int posix_memalign(void** ptr, size_t alignment, size_t size)
{
  DPRINT("posix_memalign\t: ptr = %x, align = %u, size = %u\n", ptr, alignment, size);

  if (!size)
  {
    *ptr = NULL;
    return 0;
  }

  if ((alignment != (1 << round_up_binary(alignment)))
      || (alignment % sizeof(size_t) != 0))
  {
    return -1;
  }

  if (alignment > PAGE_SIZE)
    return large_memalign(ptr, alignment, size);
  if (alignment <= sizeof (size_t))
  {
    *ptr = malloc(size);
    return 0;
  }

  char* ret = malloc(size + alignment);
  char* aligned;
  if (!ret)
    return -1;
  aligned = ret - META_SIZE + alignment;
  meta* aligned_meta = create_meta(aligned - META_SIZE, ((aligned - ret) << 16) | ALIGNED_META_MAGIC);

  *ptr = aligned_meta->data.user;
  DPRINT("==> return addr %x\n", aligned_meta->data.user);
  return 0;
}

void* memalign(size_t alignment, size_t size)
{
  void* res;
  int ret = posix_memalign(&res, alignment, size);
  if (ret != 0)
    return NULL;
  return res;
}

void* aligned_alloc(size_t alignment, size_t size)
{
  if (size % alignment != 0)
    return NULL;
  return memalign(alignment, size);
}

void* valloc(size_t size)
{
  return memalign(PAGE_SIZE, size);
}

void* pvalloc(size_t size)
{
  return memalign(PAGE_SIZE, size_to_page_number(size) * PAGE_SIZE);
}

size_t malloc_size(const void* ptr)
{
  meta* meta = get_meta(ptr);
  if (IS_ALIGNED(meta))
    meta = get_meta_from_aligned(meta);

  return GET_SIZE(meta);
}

size_t malloc_good_size(size_t size)
{
  return internal_size(size) - META_SIZE;
}
