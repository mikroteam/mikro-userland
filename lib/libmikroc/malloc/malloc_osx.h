/*
 * File: malloc_osx.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Zone API on OSX
 *
 */

#ifndef MALLOC_OSX_H_
# define MALLOC_OSX_H_

# include <malloc/malloc.h>

void* mk_zone_malloc(malloc_zone_t *zone, size_t size);
void mk_zone_free(malloc_zone_t *zone, void* ptr);
void mk_zone_destroy(malloc_zone_t *zone);
void* mk_zone_calloc(malloc_zone_t *zone, size_t num_items, size_t size);
void* mk_zone_valloc(malloc_zone_t *zone, size_t size);
void* mk_zone_realloc(malloc_zone_t *zone, void *ptr, size_t size);
void* mk_zone_memalign(malloc_zone_t *zone, size_t alignment, size_t size);
size_t mk_zone_size(malloc_zone_t *zone, const void* ptr);
void mk_zone_force_lock(malloc_zone_t* z);
void mk_zone_force_unlock(malloc_zone_t* z);
boolean_t mk_zone_locked(malloc_zone_t* z);
size_t mk_zone_good_size(malloc_zone_t* zone, size_t size);
void mk_zone_free_definite_size(malloc_zone_t *zone, void* ptr, size_t size);
kern_return_t mk_zone_enumerator(task_t task, void *p, unsigned type_mask,
                                 vm_address_t zone_address,
                                 memory_reader_t reader,
                                 vm_range_recorder_t recorder);
boolean_t mk_zone_check(malloc_zone_t *zone);
void mk_zone_print(malloc_zone_t *zone, boolean_t verbose);
void mk_zone_log(malloc_zone_t *zone, void *address);
void mk_zone_statistics(malloc_zone_t *zone, malloc_statistics_t *stats);

#endif /* !MALLOC_OSX_H_ */
