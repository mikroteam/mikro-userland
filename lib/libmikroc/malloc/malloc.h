/*
 * File: malloc.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Simple and portable malloc implementation
 *
 */

#ifndef MALLOC_H_
# define MALLOC_H_

# include <stddef.h>

# ifdef USE_PREFIX
#  define malloc mk_malloc
#  define free mk_free
#  define realloc mk_realloc
#  define reallocf mk_reallocf
#  define calloc mk_calloc
#  define posix_memalign mk_posix_memalign
#  define memalign mk_memalign
#  define aligned_alloc mk_aligned_alloc
#  define valloc mk_valloc
#  define pvalloc mk_pvalloc
#  define malloc_size mk_malloc_size
#  define malloc_good_size mk_malloc_good_size
# endif

void* malloc(size_t size);
void free(void*);
void* realloc(void* ptr, size_t size);
void* reallocf(void* ptr, size_t size);
void* calloc(size_t nmenb, size_t size);
int posix_memalign(void** ptr, size_t alignment, size_t size);
void* memalign(size_t alignment, size_t size);
void* aligned_alloc(size_t alignment, size_t size);
void* valloc(size_t size);
void* pvalloc(size_t size);
size_t malloc_size(const void* ptr);
size_t malloc_good_size(size_t size);

#endif /* !MALLOC_H_ */
