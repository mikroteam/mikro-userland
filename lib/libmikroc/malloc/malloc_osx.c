/*
 * File: malloc_osx.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Zone API on OSX
 *
 */

#include <string.h>

#include "malloc.h"
#include "malloc_internal.h"
#include "malloc_debug.h"
#include "malloc_osx.h"

static malloc_zone_t mk_malloc_zone;
static malloc_introspection_t mk_malloc_introspection;
static char is_locked = 0;

__attribute__((constructor)) void mk_zone_init()
{
  // Inspired from Google's gperftools trick
  memset(&mk_malloc_introspection, 0, sizeof(mk_malloc_introspection));
  memset(&mk_malloc_zone, 0, sizeof(malloc_zone_t));

  mk_malloc_introspection.enumerator = &mk_zone_enumerator;
  mk_malloc_introspection.good_size = &mk_zone_good_size;
  mk_malloc_introspection.check = &mk_zone_check;
  mk_malloc_introspection.print = &mk_zone_print;
  mk_malloc_introspection.log = &mk_zone_log;
  mk_malloc_introspection.force_lock = &mk_zone_force_lock;
  mk_malloc_introspection.force_unlock = &mk_zone_force_unlock;

  mk_malloc_zone.version = 6;
  mk_malloc_zone.zone_name = "mk_malloc";
  mk_malloc_zone.size = &mk_zone_size;
  mk_malloc_zone.malloc = &mk_zone_malloc;
  mk_malloc_zone.calloc = &mk_zone_calloc;
  mk_malloc_zone.valloc = &mk_zone_valloc;
  mk_malloc_zone.free = &mk_zone_free;
  mk_malloc_zone.realloc = &mk_zone_realloc;
  mk_malloc_zone.destroy = &mk_zone_destroy;
  mk_malloc_zone.batch_malloc = NULL;
  mk_malloc_zone.batch_free = NULL;
  mk_malloc_zone.introspect = &mk_malloc_introspection;

  mk_malloc_zone.free_definite_size = &mk_zone_free_definite_size;
  mk_malloc_zone.memalign = &mk_zone_memalign;
  mk_malloc_introspection.zone_locked = &mk_zone_locked;

  if (malloc_default_purgeable_zone)
    malloc_default_purgeable_zone();

  // Do the magic: swap default zone and custom zone
  malloc_zone_register(&mk_malloc_zone);
  malloc_zone_t *default_zone = malloc_default_zone();
  malloc_zone_unregister(default_zone);
  malloc_zone_register(default_zone);
}

kern_return_t mk_zone_enumerator(task_t task, void *p, unsigned type_mask,
                                 vm_address_t zone_address,
                                 memory_reader_t reader,
                                 vm_range_recorder_t recorder)
{
  (void)task;
  (void)p;
  (void)type_mask;
  (void)zone_address;
  (void)reader;
  (void)recorder;
  return KERN_FAILURE;
}

boolean_t mk_zone_check(malloc_zone_t *zone)
{
  (void)zone;
  return 1;
}

void mk_zone_print(malloc_zone_t *zone, boolean_t verbose)
{
  (void)zone;
  (void)verbose;
}

void mk_zone_log(malloc_zone_t *zone, void *address)
{
  (void)zone;
  (void)address;
}

void mk_zone_statistics(malloc_zone_t *zone, malloc_statistics_t *stats)
{
  (void)zone;
  (void)stats;
}

void mk_zone_force_lock(malloc_zone_t* z)
{
  (void)z;
  MALLOC_LOCK;
  is_locked = 1;
}

void mk_zone_force_unlock(malloc_zone_t* z)
{
  (void)z;
  MALLOC_UNLOCK;
  is_locked = 0;
}

boolean_t mk_zone_locked(malloc_zone_t* z)
{
  (void)z;
  return is_locked;
}

size_t mk_zone_good_size(malloc_zone_t* zone, size_t size)
{
  (void)zone;
  return malloc_good_size(size);
}

size_t mk_zone_size(malloc_zone_t *zone, const void* ptr)
{
  (void)zone;
  return malloc_size(ptr);
}

void *mk_zone_malloc(malloc_zone_t *zone, size_t size)
{
  (void)zone;
  return malloc(size);
}

void mk_zone_free(malloc_zone_t *zone, void* ptr)
{
  (void)zone;
  free(ptr);
}

void mk_zone_free_definite_size(malloc_zone_t *zone, void* ptr, size_t size)
{
  // FIXME: check size here ?
  (void)size;
  mk_zone_free(zone, ptr);
}

void mk_zone_destroy(malloc_zone_t *zone)
{
  (void)zone;
}

void* mk_zone_calloc(malloc_zone_t *zone, size_t num_items, size_t size)
{
  (void)zone;
  return calloc(num_items, size);
}

void* mk_zone_valloc(malloc_zone_t *zone, size_t size)
{
  (void)zone;
  return valloc(size);
}

void* mk_zone_realloc(malloc_zone_t *zone, void *ptr, size_t size)
{
  (void)zone;
  return realloc(ptr, size);
}

void* mk_zone_memalign(malloc_zone_t *zone, size_t alignment, size_t size)
{
  (void)zone;
  return memalign(alignment, size);
}
