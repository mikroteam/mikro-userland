/*
 * File: mman.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description:
 *
 */

#include <sys/mman.h>
#include <mikro/mikro.h>

void* mmap(void *addr, size_t len, int prot, int flags, int fd, off_t offset)
{
  (void)fd;
  (void)flags;
  (void)offset;

  if (!len)
    return (void *) 0;

  return mikro_mmap(addr, ((len - 1) / (1 << PAGE_SHIFT)) + 1, prot);
}

int munmap(void *addr, size_t len)
{
  (void)len;

  return !mikro_munmap(addr);
}
