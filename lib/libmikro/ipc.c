/**
 * \file ipc.c
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Mikro IPC basic functionalities
 */

#include <mikro/mikro.h>
#include <mikro/syscall.h>

int channel_create(const char* name, u32 flags)
{
  int res;
  SYSCALL2(SYS_CHANNEL_CREATE, res, name, flags);

  return res;
}

int channel_close(int ch)
{
  int res;
  SYSCALL1(SYS_CHANNEL_CLOSE, res, ch);

  return res;
}

int channel_open(const char* name, u32 flags)
{
  int res;
  SYSCALL2(SYS_CHANNEL_OPEN, res, name, flags);

  return res;
}

int channel_send(int ch, const void* msg, size_t size, u32 flags)
{
  int res;
  SYSCALL4(SYS_CHANNEL_SEND, res, ch, msg, size, flags);

  return res;
}

int channel_recv(int ch, void* msg, size_t size, u32 flags)
{
  int res;
  SYSCALL4(SYS_CHANNEL_RECV, res, ch, msg, size, flags);

  return res;
}

// TODO
#if 0
int channel_srecv(int ch, void* msg_out, size_t size_out, void* msg_in,
    size_t size_in, u32 flags)
{

}
#endif

int channel_listen(int ch, void* msg, size_t size, struct mhandle** mh,
    u32 flags)
{
  int res, pad;
  SYSCALL4(SYS_CHANNEL_RECV, res, ch, msg, size, flags);

  if (res > 0)
  {
    pad = res % sizeof(size_t);

    if (pad)
      *mh = (struct mhandle *) ((char *) msg + res + sizeof(size_t) - pad);
    else
      *mh = (struct mhandle *) ((char *) msg + res);
  }

  return res;
}

int channel_reply_close(int ch, struct mhandle* mh, const void* msg,
    size_t size, u32 flags)
{
  int res;
  size_t mhId = mh->mh & ~(MH_PID_BIT | MH_DEL_BIT);

  SYSCALL5(SYS_CHANNEL_REPLY_CLOSE, res, ch, mhId, msg, size, flags);

  return res;
}

int channel_send_event(int ch, pid_t pid, const void* msg, size_t size,
    u32 flags)
{
  int res;
  SYSCALL5(SYS_CHANNEL_SEND_EVENT, res, ch, pid, msg, size, flags);

  return res;
}

int channel_change_private(int ch, struct mhandle* mh, size_t dprivate)
{
  int res;
  size_t mhId = mh->mh & ~(MH_PID_BIT | MH_DEL_BIT);

  SYSCALL3(SYS_CHANNEL_CHANGE_PRIVATE, res, ch, mhId, dprivate);

  if (!res)
    mh->dprivate = dprivate;

  return res;
}
