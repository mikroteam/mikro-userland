/**
 * \file debug.c
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Debug syscall
 */

#include <mikro/mikro.h>
#include <mikro/syscall.h>

int mikro_debug(const char *str)
{
  int res;
  SYSCALL1(SYS_DEBUG, res, str);

  return res;
}
