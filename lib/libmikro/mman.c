/*
 * File: mman.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description:
 *
 */

#include <mikro/mikro.h>
#include <mikro/syscall.h>

void* mikro_mmap(void* vaddr, const size_t nbPages, u16 flags)
{
  void* res;
  SYSCALL3(SYS_MMAP, res, vaddr, nbPages, flags);

  return res;
}

void* mikro_mmap_phys(void* vaddr, void *paddr, const size_t nbPages,
    u16 flags)
{
  void* res;
  SYSCALL4(SYS_MMAP_PHYS, res, vaddr, paddr, nbPages, flags);

  return res;
}

int mikro_munmap(void* vaddr)
{
  int res;
  SYSCALL1(SYS_MUNMAP, res, vaddr);

  return res;
}
