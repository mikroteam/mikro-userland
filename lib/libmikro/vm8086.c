/**
 * \file vm8086.c
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Mikro virtual 8086 syscall
 */

#include <mikro/mikro.h>
#include <mikro/syscall.h>

int mikro_vm8086(struct VM86Regs *regs, u8 intNum)
{
  int res;

  SYSCALL2(SYS_VM8086, res, regs, intNum);
  return res;
}

