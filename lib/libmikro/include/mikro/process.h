/*
 * File: process.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Process handling
 *
 */

#ifndef PROCESS_H_
# define PROCESS_H_

# define CHANDLE_BLANK_AS -3

# define SIGNAL_KILL 0x1

/**
 * \fn process_create
 * \brief Create a new process in kernel with the specified address space
 *
 * Process with a blank as will start in receive mode waiting for a send
 * with arguments (argc, argv, env).
 *
 * \param ch Chandle of the process to share address space
 * \param entry_point Entry point of the process
 * \param stack_addr Address of the stack
 * \param stack_size Size of the stack
 * \param flags Options -- Not for the moment
 *
 * \return < 0 on error, chandle otherwise
 *
 */
int process_create(int ch, void* entry_point, void* stack_addr, size_t stack_size, u32 flags);

/**
 * \fn process_exit
 * \brief Exit the current thread
 *
 */
void process_exit();

/**
 * \fn process_signal
 * \brief Send a signal to a process
 *
 * \param pid Process id
 * \param signal Signal to send
 *
 * \return < 0 on error, 0 on success
 *
 */
int process_signal(pid_t pid, int signal);

#endif /* !PROCESS_H_ */
