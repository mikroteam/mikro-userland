/**
 * \file mikro.h
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Mikro kernel basic functionalities
 */

#ifndef MIKRO_H_
#define MIKRO_H_

#include "errors.h"
#include "ipc.h"
#include "types.h"

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

/**
 * \fn mikro_debug
 * \brief Print a debug message using kernel logger
 * Only available when kernel is in not in release mode
 *
 * \param str pointer to string to display
 * \return 0
 */
int mikro_debug(const char *str);

/**
 * \fn mikro_mmap
 * \brief Map a new page in current address space
 *
 * \param vaddr desired addr
 * \param nbPages number of pages to map
 * \param flags rights requested on pages
 * \return addr of the new mapped page
 */
void* mikro_mmap(void* vaddr, const size_t nbPages, u16 flags);

/**
 * \fn mikro_mmap_phys
 * \brief Map a physical page in current address space
 *
 * \param vaddr desired virtual addr
 * \param paddr desired physical addr
 * \param nbPages number of pages to map
 * \param flags rights requested on pages
 * \return addr of the new mapped page
 */
void* mikro_mmap_phys(void* vaddr, void *paddr, const size_t nbPages,
    u16 flags);

/**
 * \fn mikro_munmap
 * \brief Unmap a page in current address space
 *
 * \param vaddr page to unmap
 * \return < 0 if something went wrong
 */
int mikro_munmap(void* vaddr);

struct VM86Regs
{
  u16 di;
  u16 si;
  u16 bp;
  u16 bx;
  u16 dx;
  u16 cx;
  u16 ax;
};

/**
 * \fn mikro_vm8086
 * \brief Virtual 8086 interrupt syscall
 *
 * \param regs V8086 registries set before interrupt
 * \param intNum V8086 interrupt number
 * \return < 0 if something went wrong
 */
int mikro_vm8086(struct VM86Regs *regs, u8 intNum);

/**
 * \fn mikro_in
 * \brief in - x86 in io port
 *
 * \param port IO port
 * \param size 1,2,4 size of the value to retrieve
 * \return value asked on IO port
 */
int mikro_in(u16 port, u8 size);

/**
 * \fn mikro_in
 * \brief in - x86 in io port
 *
 * \param port IO port
 * \param value value to output
 * \param size 1,2,4 size of the value to retrieve
 * \return < 0 on error
 */
int mikro_out(u16 port, u32 value, u8 size);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !MIKRO_H_ */
