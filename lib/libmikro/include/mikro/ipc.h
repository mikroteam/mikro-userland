/*
 * File: ipc.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Interprocess communication
 *
 */

#ifndef IPC_H_
# define IPC_H_

#include "types.h"

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

# define CHANNEL_ONE 0x0
# define CHANNEL_SERVER 0x1

# define CHANNEL_INVALID -1
# define CHANNEL_FROM_ALL -2

# define IPC_RECV_ALL 0x0
# define IPC_RECV_COPY_ONLY 0x1

# define MH_PID_BIT (1 << (sizeof(size_t) * 8 - 1))
# define MH_DEL_BIT (1 << (sizeof(size_t) * 8 - 2))

# define channel_get_private(mh) (mh->dprivate)
# define channel_isPid_private(mh) (mh->mh & MH_PID_BIT)
# define channel_isDel_private(mh) (mh->mh & MH_DEL_BIT)


struct mhandle
{
  int mh; // mhandle
  size_t dprivate;
};

/* --------------------- Common --------------------- */
/**
 * \fn channel_create
 * \brief Create a channel to exchange between processes
 *
 * \param name Name of the process
 * \param flags Options associated with the channel
 *
 * \return < 0 on error, chandle otherwise
 *
 */
int channel_create(const char* name, u32 flags);

/**
 * \fn channel_close
 * \brief Close a channel
 *
 * \param ch chandle
 *
 * \return < 0 on error, 0 on success
 *
 */
int channel_close(int ch);

/* --------------------- Client --------------------- */
/**
 * \fn channel_open
 * \brief Open a channel as a client
 *
 * \param name Name of the process
 * \param flags Options associated with the channel
 *
 * \return < 0 on error, chandle otherwise
 *
 */
int channel_open(const char* name, u32 flags);

/**
 * \fn channel_send
 * \brief Send a message to a process
 *
 * \param ch chandle
 * \param msg buffer message, cannot be NULL
 * \param size message size
 * \param flags send flags
 *
 * \return < 0 on error, chandle otherwise
 *
 */
int channel_send(int ch, const void* msg, size_t size, u32 flags);

/**
 * \fn channel_recv
 * \brief Receive a message from a process
 *
 * \param ch chandle (can be CHANNEL_FROM_ALL)
 * \param msg buffer message, cannot be NULL. This parameter can be changed
 *        in case of map shared ipc.
 * \param size buffer size, message size on success
 * \param flags options for the IPC
 *
 * \return < 0 on error, chandle otherwise
 *
 */
int channel_recv(int ch, void* msg, size_t size, u32 flags);

/**
 * \fn channel_srecv
 * \brief Send a message to a process and receive
 *
 * This is equivalent to channel_send and then channel_recv but
 * optimized for better performance.
 *
 * \param ch chandle (can be CHANNEL_FROM_ALL)
 * \param msg_out buffer message for output, cannot be NULL
 * \param size_out buffer size for output, message size on success
 * \param msg_in buffer message for input, cannot be NULL
 * \param size_in buffer size for input, message size on success
 * \param flags send/receive flags
 *
 * \return < 0 on error, chandle otherwise
 *
 */
int channel_srecv(int ch, void* msg_out, size_t size_out, void* msg_in,
    size_t size_in, u32 flags);

/* --------------------- Server --------------------- */
/**
 * \fn channel_listen
 * \brief Listen to a message from clients
 *
 * \param ch chandle (can be CHANNEL_FROM_ALL)
 * \param msg buffer message, cannot be NULL. This parameter can be changed
 *        in case of map shared ipc.
 * \param size buffer size, message size on success
 * \param mh mhandle, cannot be NULL
 * \param flags options for the IPC
 *
 * \return < 0 on error, chandle on success
 *
 */
int channel_listen(int ch, void* msg, size_t size, struct mhandle** mh,
    u32 flags);

/**
 * \fn channel_reply_close
 * \brief Reply to a client and close the mhandle
 *
 * \param ch chandle
 * \param mh mhandle, cannot be NULL
 * \param msg buffer message, can be NULL if the server doesn't want to reply
 * \param size buffer size, can be zero if the server doesn't want to reply
 * \param flags send flags
 *
 * \return < 0 on error, 0 on success
 *
 */
int channel_reply_close(int ch, struct mhandle* mh, const void* msg,
    size_t size, u32 flags);

/**
 * \fn channel_send_event
 * \brief Send an event to a client
 *
 * \param ch chandle
 * \param pid process id
 * \param msg buffer message, cannot be NULL
 * \param size buffer size
 * \param flags send flags
 *
 * \return < 0 on error, 0 on success
 *
 */
int channel_send_event(int ch, pid_t pid, const void* msg, size_t size,
    u32 flags);

/**
 * \fn channel_change_private
 * \brief Change private data associated with a client
 *
 * \param ch chandle
 * \param mh mhandle
 * \param dprivate new private data
 *
 * \return < 0 on error, 0 on success
 *
 */
int channel_change_private(int ch, struct mhandle* mh, size_t dprivate);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !IPC_H_ */
