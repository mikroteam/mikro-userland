/**
 * \file syscall.h
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Mikro syscall helper
 */

#ifndef SYSCALL_H_
#define SYSCALL_H_

/* For Intel x86 CPU */
#ifdef __i386__

/* SYSCALL ID */
#define SYS_MMAP                    0
#define SYS_MMAP_PHYS               1
#define SYS_MUNMAP                  2
#define SYS_MAP_CHANGE_RIGHTS       3

#define SYS_CHANNEL_CREATE          4
#define SYS_CHANNEL_CLOSE           5
#define SYS_CHANNEL_OPEN            6
#define SYS_CHANNEL_SEND            7
#define SYS_CHANNEL_RECV            8
#define SYS_CHANNEL_SRECV           9
#define SYS_CHANNEL_REPLY_CLOSE     10
#define SYS_CHANNEL_SEND_EVENT      11
#define SYS_CHANNEL_CHANGE_PRIVATE  12

#define SYS_IN                      13
#define SYS_OUT                     14
#define SYS_VM8086                  15
#define SYS_DEBUG                   16


/* SYSCALL MACROS */
#define MIKRO_SYSCALL_INT_NUM 0x80

#define ASM_INT_MACRO1(num) "int $"#num"\n"
#define ASM_INT_MACRO2(num) ASM_INT_MACRO1(num)
#define ASM_INT ASM_INT_MACRO2(MIKRO_SYSCALL_INT_NUM)

#define SYSCALL0(num, ret)  \
  __asm__ (                 \
      "mov %1, %%eax\n"      \
      ASM_INT               \
      : "=a" (ret)          \
      : "i"(num)            \
      )

#define SYSCALL1(num, ret, p1)  \
  __asm__ (                     \
      "mov %1, %%eax\n"          \
      ASM_INT                   \
      : "=a" (ret)              \
      : "i"(num), "b"(p1)       \
      )

#define SYSCALL2(num, ret, p1, p2)  \
  __asm__ (                         \
      "mov %1, %%eax \n"             \
      ASM_INT                       \
      : "=a" (ret)                  \
      : "i"(num), "b"(p1), "c"(p2)  \
      )



#define SYSCALL3(num, ret, p1, p2, p3)      \
  __asm__ (                                 \
      "mov %1, %%eax\n"                      \
      ASM_INT                               \
      : "=a" (ret)                          \
      : "i"(num), "b"(p1), "c"(p2), "d"(p3) \
      )

#define SYSCALL4(num, ret, p1, p2, p3, p4)            \
  __asm__ (                                           \
      "mov %1, %%eax\n"                                \
      ASM_INT                                         \
      : "=a" (ret)                                    \
      : "i"(num), "b"(p1), "c"(p2), "d"(p3), "S"(p4)  \
      )

#define SYSCALL5(num, ret, p1, p2, p3, p4, p5)                \
  __asm__ (                                                   \
      "mov %1, %%eax\n"                                        \
      ASM_INT                                                 \
      : "=a" (ret)                                            \
      : "i"(num), "b"(p1), "c"(p2), "d"(p3), "S"(p4), "D"(p5) \
      )

#endif /* __i386__ */

#endif /* !SYSCALL_H_ */
