/**
 * \file errors.h
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief mikro error codes
 */

#ifndef ERRORS_H_
#define ERRORS_H_

#define ENOMEM  1   /* Memory error */
#define EPERM   2   /* Operation not permitted */
#define EINVAL  3   /* Invalid argument */
#define ESRCH   4   /* No such process */
#define EFAULT  5   /* Bad address */
#define ECHAN   6   /* Channel error */
#define ECHAND  7   /* Chandle error */
#define EMHAND  8   /* Mhandle error */
#define EMSIZE  9   /* Message size error */
#define ENOPID  10  /* Too many pid created */

#endif /* !ERRORS_H_ */
