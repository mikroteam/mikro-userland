/*
 * File: io.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: IN/OUT on x86
 *
 */

#include <mikro/mikro.h>
#include <mikro/syscall.h>

int mikro_in(u16 port, u8 size)
{
  int res;

  SYSCALL2(SYS_IN, res, port, size);
  return res;
}

int mikro_out(u16 port, u32 value, u8 size)
{
  int res;

  SYSCALL3(SYS_OUT, res, port, value, size);
  return res;
}
