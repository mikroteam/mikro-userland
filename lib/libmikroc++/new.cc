/*
 * File: new.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: new and delete operators
 *
 */

#include <stdlib.h>

void *operator new(size_t size) noexcept
{
  return malloc(size);
}

void *operator new[](size_t size) noexcept
{
  return malloc(size);
}

void operator delete(void *p)
{
  free(p);
}

void operator delete[](void *p)
{
  free(p);
}
