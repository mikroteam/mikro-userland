mikro-userland
==============

Userland for mikro, this project contains drivers,
libs and applications for the mikro kernel.

Daemons:
--------
- vfsd: Virtual File System Daemon
    - ext2 support (read only)
- vesa: print on screen using vesa specification

Libs:
-----
- libmikro: communication with the kernel
- minimal libc
- ultra minimal libc++

Founders:
---------
Julien Freche, Victor Apercé
